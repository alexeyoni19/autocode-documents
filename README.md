# Autocode documents

## Adding documents to Autocode

You can add pictures and working links to Autocode using document full url from Gitlab public repository.

### Follow next steps:
- In `README` in "Before we start" description use images and documents from `HTML-CSS_Autocode_template_docs` with their full url

For example:
![image example](https://gitlab.com/gap-bs-front-end-autocode-documents/autocode-documents/-/raw/main/Autocode%20template%20docs/images/tests-run.PNG)

- Add new folder for your topic with images you want to add to autocode. Commit your changes and push on Gitlab.
- Copy image url from Gitlab and use it in your task description in `README`

For example:
![image example](https://gitlab.com/gap-bs-front-end-autocode-documents/autocode-documents/-/raw/main/CSS%20Positioning%20and%20layouts/CSS%20Flexbox/header-nav.PNG)

- Import `README` from your repository as an Autocode description. All provided images will be displayed in task description and all links on external documents should work.
